use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_TABLE as G, ristretto::RistrettoPoint, scalar::Scalar,
};
use transcript_hashing::Transcript;

#[derive(Default, Clone, Copy)]
pub struct SecretShare {
    pub index: u64,
    pub secret: Scalar,
}

impl SecretShare {
    pub fn public(&self) -> PublicShare {
        PublicShare {
            index: self.index,
            point: &self.secret * &G,
        }
    }
}

#[derive(Default)]
pub struct PublicShare {
    pub index: u64,
    pub point: RistrettoPoint,
}

fn gen_shares_inner<D: transcript_hashing::Digest>(
    ts: &mut Transcript<D>,
    coeffs: &mut [Scalar],
    commits: &mut [RistrettoPoint],
    shares: &mut [SecretShare],
) {
    assert!(shares.len() >= coeffs.len(), "");

    // avoid marking the transcript as dirty with each commitment
    let mut ts_rand_fork = ts.fork();

    for (coeff, commit) in coeffs.iter_mut().zip(commits) {
        *coeff = ts_rand_fork.random();
        *commit = ts.append(&*coeff * &G);
    }

    for (SecretShare { index, secret }, i) in shares.iter_mut().zip(1u64..) {
        let si = Scalar::from(i);
        let mut sum = Scalar::zero();

        for c in coeffs[1..].iter().rev() {
            sum += c;
            sum *= si;
        }

        *index = i;
        *secret = sum + coeffs[0];
    }
}

pub fn gen_shares<D: transcript_hashing::Digest, const K: usize, const N: usize>(
    ts: &mut Transcript<D>,
) -> (Scalar, [RistrettoPoint; K], [SecretShare; N])
where
    [Scalar; K]: Sized,
    [RistrettoPoint; K]: Sized,
    [SecretShare; N]: Sized,
{
    let mut coeffs = [Default::default(); K];
    let mut commits = [Default::default(); K];
    let mut shares = [Default::default(); N];

    gen_shares_inner(ts, &mut coeffs, &mut commits, &mut shares);

    (coeffs[0], commits, shares)
}

#[cfg(feature = "std")]
pub struct ShamirFeldman {
    coeffs: Vec<Scalar>,
    commits: Vec<RistrettoPoint>,
    shares: Vec<SecretShare>,
}

#[cfg(feature = "std")]
impl ShamirFeldman {
    pub fn new(k: usize, n: usize) -> Self {
        Self {
            coeffs: vec![Default::default(); k],
            commits: vec![Default::default(); k],
            shares: vec![Default::default(); n],
        }
    }

    pub fn reconfigure(&mut self, k: usize, n: usize) {
        self.coeffs.resize(k, Default::default());
        self.commits.resize(k, Default::default());
        self.shares.resize(n, Default::default());
    }

    /// Split a secret key into N shares where any subset K shares are sufficient
    /// to reconstruct the initial secret.
    pub fn gen_shares<D: transcript_hashing::Digest>(
        &mut self,
        ts: &mut Transcript<D>,
    ) -> (&Scalar, &[RistrettoPoint], &[SecretShare]) {
        gen_shares_inner(ts, &mut self.coeffs, &mut self.commits, &mut self.shares);

        (&self.coeffs[0], &self.commits, &self.shares)
    }
}

/// Coalesce K (or more) scalars
pub fn coalesce_scalars(shares: &[SecretShare]) -> Scalar {
    let mut r = Scalar::zero();

    // compute over-estimation for the common numerator
    let num = shares
        .iter()
        .map(|s| Scalar::from(s.index))
        .product::<Scalar>();
    // Only safe if indices are trusted in-bounds.
    // let num = Scalar::from(shares.iter().fold(1, |a, b| a * b.index));

    for SecretShare { index, secret } in shares {
        let si = Scalar::from(*index);
        // cancel the over-estimation above
        let mut den = si;
        for SecretShare { index: j, .. } in shares {
            if j != index {
                den *= Scalar::from(*j) - si;
            }
        }
        // FIXME: batch scalar inverses
        r += secret * num * den.invert();
    }

    r
}

/// Coalesce K (or more) points
pub fn coalesce_points(shares: &[PublicShare]) -> RistrettoPoint {
    let mut r = &G * &Scalar::zero();

    // precompute common numerator (over-estimation)
    let num = shares
        .iter()
        .map(|s| Scalar::from(s.index))
        .product::<Scalar>();

    for PublicShare { index, point } in shares {
        let si = Scalar::from(*index);
        // cancels out the over-estimation above
        let mut den = si;
        for PublicShare { index: j, .. } in shares {
            if j != index {
                den *= Scalar::from(*j) - si;
            }
        }
        // FIXME: batch scalar inverses
        r += num * den.invert() * point;
    }

    r
}

/// Verify an individual share using Feldman's VSS
pub fn validate(commits: &[RistrettoPoint], SecretShare { index, secret }: &SecretShare) -> bool {
    let si = Scalar::from(*index);
    let mut sum = &G * &Scalar::zero();

    for c in commits[1..].iter().rev() {
        sum += c;
        sum *= si;
    }

    (sum + commits[0]).compress() == (secret * &G).compress()
}
