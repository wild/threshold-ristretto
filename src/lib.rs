#![no_std]
#![forbid(unsafe_code)]
#![warn(missing_docs)]
#![allow(non_snake_case)]

//! Common building blocks for threshold cryptosystems using the Ristretto255
//! prime-order group.

/// Shamir Secret Sharing Scheme / Feldman's Verifiable Secret Sharing
pub mod shamir_feldman;

/// CCA-secure labeled threshold encryption.
///
/// TDH1 assumes: CDH
/// TDH2 assumes: DDH
///
/// 'Securing Threshold Cryptosystems against Chosen Ciphertext Attack'
/// - Victor Shoup and Rosario Gennaro. September 18, 2001.
///
/// https://www.shoup.net/papers/thresh1.pdf
pub mod tdh2;

/// Diffie-Hellman Based Threshold Coin-Tossing Scheme.
///
/// Assumes: CDH in the ROM if k = t + 1, DDH otherwise
///
/// Random Oracles in Constantinople: Practical Asynchronous Byzantine Agreement using Cryptography
/// - Christian Cachin, Klaus Kursawe, Victor Shoup. August 14, 2000
///
/// https://eprint.iacr.org/2000/034
pub mod coin_toss;
