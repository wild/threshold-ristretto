use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_TABLE as G, ristretto::RistrettoPoint, scalar::Scalar,
};
use transcript_hashing::Transcript;

use crate::shamir_feldman::{coalesce_points, PublicShare, SecretShare};

pub struct Ciphertext {
    c: [u8; 32],
    rG: RistrettoPoint,
    rH: RistrettoPoint,
    e: Scalar,
    f: Scalar,
}

pub struct ValidCt {
    c: [u8; 32],
    rG: RistrettoPoint,
}

pub struct Share {
    i: u64,
    xrG: RistrettoPoint,
    e_i: Scalar,
    f_i: Scalar,
}

pub struct PublicKey {
    pub kG: RistrettoPoint,
    pub H: RistrettoPoint,
}

impl PublicKey {
    /// Encrypt a message bound to a transcript. As defined, TDH2 asks for a
    /// "label" which represents a "decryption policy". The transcript subsumes
    /// the label.
    pub fn encrypt<D: transcript_hashing::Digest>(
        &self,
        ts: &mut Transcript<D>,
        m: &[u8; 32],
    ) -> Ciphertext {
        let PublicKey { kG, H } = self;

        let s: Scalar = ts.random();
        let r: Scalar = ts.random();

        let mut ts_final = ts.fork();

        ts_final.append((&r * kG).compress());
        let mut c: [u8; 32] = ts_final.challenge(b"c");
        for (a, b) in c.iter_mut().zip(m) {
            *a ^= b;
        }

        ts.append(kG);
        ts.append(&c);
        let rG = ts.append(&G * &r);
        ts.append(&G * &s);
        let rH = ts.append(H * r);
        ts.append(H * s);

        let e: Scalar = ts.challenge(b"e");
        let f = &s + &r * &e;

        Ciphertext { c, rG, rH, e, f }
    }

    pub fn validate_ct<D: transcript_hashing::Digest>(
        &self,
        ts: &mut Transcript<D>,
        Ciphertext { c, rG, rH, e, f }: Ciphertext,
    ) -> Result<ValidCt, ()> {
        let PublicKey { kG, H } = self;

        ts.append(kG);
        ts.append(&c);
        ts.append(rG);
        // sG
        ts.append(&G * &f - rG * e);
        ts.append(rH);
        // sH
        ts.append(H * &f - rH * e);
        let e2: Scalar = ts.challenge(b"e");

        if e == e2 {
            Ok(ValidCt { c, rG })
        } else {
            Err(())
        }
    }
}

pub fn decrypt_share<D: transcript_hashing::Digest>(
    ts: &mut Transcript<D>,
    SecretShare { index, secret }: &SecretShare,
    ValidCt { rG, .. }: &ValidCt,
) -> Share {
    ts.witness(secret);
    let s: Scalar = ts.random();

    ts.append(index);
    ts.append(rG);
    let xrG = ts.append(secret * rG);
    ts.append(rG * s);
    ts.append(&G * &s);

    let e_i: Scalar = ts.challenge(b"e_i");
    let f_i = s + secret * e_i;

    Share {
        i: *index,
        xrG,
        e_i,
        f_i,
    }
}

pub fn validate_share<D: transcript_hashing::Digest>(
    ts: &mut Transcript<D>,
    h: &[RistrettoPoint],
    ValidCt { rG, .. }: &ValidCt,
    Share { i, xrG, e_i, f_i }: Share,
) -> Result<PublicShare, ()> {
    let h = h.get(i as usize - 1).ok_or(())?;

    ts.append(i);
    ts.append(rG);
    ts.append(xrG);
    ts.append(rG * f_i - xrG * e_i);
    ts.append(&G * &f_i - h * e_i);
    let e2: Scalar = ts.challenge(b"e_i");

    if e_i == e2 {
        Ok(PublicShare {
            index: i,
            point: xrG,
        })
    } else {
        Err(())
    }
}

pub fn decrypt<D: transcript_hashing::Digest>(
    ts: &mut Transcript<D>,
    shares: &[PublicShare],
    ValidCt { c, .. }: ValidCt,
) -> [u8; 32] {
    ts.append(&coalesce_points(&shares));
    let mut m: [u8; 32] = ts.challenge(b"c");

    for (a, b) in m.iter_mut().zip(&c) {
        *a ^= b;
    }

    m
}
