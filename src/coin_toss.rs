use curve25519_dalek::{
    constants::RISTRETTO_BASEPOINT_TABLE as G, ristretto::RistrettoPoint, scalar::Scalar,
};
use transcript_hashing::Transcript;

use crate::shamir_feldman::{coalesce_points, PublicShare, SecretShare};

pub struct Share {
    i: u64,
    xH: RistrettoPoint,
    c: Scalar,
    z: Scalar,
}

pub fn compute_share<D: transcript_hashing::Digest>(
    ts: &mut Transcript<D>,
    SecretShare { secret, index }: &SecretShare,
) -> Share {
    // Witness our secret key before generating a random nonce.
    ts.witness(secret);
    let s: Scalar = ts.random();

    // The coin being flipped /is the transcript/.
    let H: RistrettoPoint = ts.challenge(b"H");

    ts.append(&G * secret);
    ts.append(&G * &s);
    ts.append(H * s);
    let xH = ts.append(H * secret);
    let c: Scalar = ts.challenge(b"c");
    let z = s + secret * c;

    Share {
        i: *index,
        xH,
        c,
        z,
    }
}

pub fn validate_share<D: transcript_hashing::Digest>(
    ts: &mut Transcript<D>,
    h: &[RistrettoPoint],
    Share { i, xH, c, z }: Share,
) -> Result<PublicShare, ()> {
    let H: RistrettoPoint = ts.challenge(b"H");
    let h = ts.append(h.get(i as usize - 1).ok_or(())?);
    ts.append(&G * &z - h * c);
    ts.append(H * z - xH * c);
    ts.append(xH);
    let c2: Scalar = ts.challenge(b"c");

    if c == c2 {
        Ok(PublicShare {
            index: i,
            point: xH,
        })
    } else {
        Err(())
    }
}

pub fn coalesce<D: transcript_hashing::Digest>(
    ts: &mut Transcript<D>,
    shares: &[PublicShare],
) -> u64 {
    ts.append(coalesce_points(shares));
    ts.challenge(b"coin")
}
