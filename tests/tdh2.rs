#![forbid(unsafe_code)]
#![allow(non_snake_case)]

use curve25519_dalek::constants::RISTRETTO_BASEPOINT_TABLE as G;
use transcript_hashing::Blake3Transcript;

use threshold_ristretto::{shamir_feldman::gen_shares, tdh2::*};

#[test]
fn check() {
    let mut ts = Blake3Transcript::new_deterministic(b"An existing protocol state");

    let (_, commits, shares) = gen_shares::<_, 3, 9>(&mut ts);

    let public_key = PublicKey {
        kG: commits[0],
        H: ts.challenge(b"H"),
    };

    let h = shares.iter().map(|s| &s.secret * &G).collect::<Vec<_>>();

    let message = ts.random();
    let ciphertext = public_key.encrypt(&mut ts.fork(), &message);
    let validated_ciphertext = public_key
        .validate_ct(&mut ts.fork(), ciphertext)
        .expect("Invalid ciphertext!");

    let validated_shares = shares
        .iter()
        .map(|share| {
            validate_share(
                &mut ts.fork(),
                &h,
                &validated_ciphertext,
                decrypt_share(&mut ts.fork(), &share, &validated_ciphertext),
            )
            .expect("Invalid share!")
        })
        .collect::<Vec<_>>();

    assert_eq!(
        message,
        decrypt(&mut ts, &validated_shares, validated_ciphertext)
    );
}
