#![forbid(unsafe_code)]
#![allow(non_snake_case)]

use curve25519_dalek::{constants::RISTRETTO_BASEPOINT_TABLE as G, ristretto::RistrettoPoint};
use transcript_hashing::Blake3Transcript;

use threshold_ristretto::{coin_toss::*, shamir_feldman::gen_shares};

#[test]
fn check() {
    let mut ts = Blake3Transcript::new_deterministic(b"An existing protocol state");

    let (secret, _, shares) = gen_shares::<_, 3, 9>(&mut ts);

    let h = shares.iter().map(|s| &G * &s.secret).collect::<Vec<_>>();

    let result = coalesce(
        &mut ts.fork(),
        &shares
            .iter()
            .map(|share| {
                validate_share(&mut ts.fork(), &h, compute_share(&mut ts.fork(), share))
                    .expect("Invalid share")
            })
            .collect::<Vec<_>>(),
    );

    let H: RistrettoPoint = ts.fork().challenge(b"H");
    ts.append(&H * secret);
    let expected = ts.challenge(b"coin");

    assert_eq!(result, expected);
}
