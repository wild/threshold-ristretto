#![forbid(unsafe_code)]
#![allow(non_snake_case)]

use transcript_hashing::Blake3Transcript;

use threshold_ristretto::shamir_feldman::*;

#[test]
fn check() {
    // create an insecure deterministic transcript for consistent tests
    let mut ts = Blake3Transcript::new_deterministic(b"sanity check");

    // generate a secret and split it into 9 shares where any 3 suffice to
    // recover the secret
    let (secret, commits, shares) = gen_shares::<_, 3, 9>(&mut ts);

    // validate each share
    for share in &shares {
        assert!(validate(&commits, share));
    }

    // any combination of k (or more) public shares recover the identity
    let pub_shares = shares.iter().map(|s| s.public()).collect::<Vec<_>>();
    assert_eq!(commits[0], coalesce_points(&pub_shares[..3]));
    assert_eq!(commits[0], coalesce_points(&pub_shares));

    // too few public shares recover a random point
    assert_ne!(commits[0], coalesce_points(&pub_shares[..2]));

    // any combination of k (or more) secret shares recover the secret
    assert_eq!(secret, coalesce_scalars(&shares[..3]));
    assert_eq!(secret, coalesce_scalars(&shares));

    // too few secret shares recover a random scalar
    assert_ne!(secret, coalesce_scalars(&shares[..2]));
}
